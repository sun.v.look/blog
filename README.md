
## How to run app

```
pip install virualenv
virtualenv .venv
source .venv/bin/activate
pip install falcon
pip install gunicorn
```

```
gunicorn --reload blog.app
```

## Tests

```
pip install pytest
```

```
pytest tests/
```