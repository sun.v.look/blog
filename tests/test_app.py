import falcon
from falcon import testing
import pytest

from blog.app import api

@pytest.fixture
def client():
    return testing.TestClient(api)

def test_blog_post(client):
    
    doc = {u'title': u'title'}

    response = client.simulate_get('/post')

    assert response.json == doc
    assert response.status == falcon.HTTP_OK